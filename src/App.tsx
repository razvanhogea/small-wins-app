import React, { useEffect, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { Button, TextField } from '@mui/material';
import { v4 as uuidv4 } from 'uuid';

function App() {
  interface SmallWin {
    id: string,
    text: string,
    date: Date
  }

  const [winsCount, setWinsCount] = useState(0);
  const [smallWinInput, setSmallWinInput] = useState<string>('');
  const [winLog, setWinLog] = useState<SmallWin[]>([]);

  const handleSetInput = ((val: string) => setSmallWinInput(val));

  const handleAddWin = (() => {
    setWinsCount(prev => prev + 1);
    let newWinLog: SmallWin[] = [
      {
        id: uuidv4(),
        text: smallWinInput,
        date: new Date()
      },
      ...winLog
    ];
    setWinLog(newWinLog);
    setSmallWinInput('');
  });

  // delete button for each entry - on hover?
  // numbering within each day should start at 1?
  let day = '';
  const smallWinsList = winLog.map((win, index) => {
    let date = win.date.toDateString();
    let display = false;
    
    if (date !== day) {
      display = true;
      day = date;
    }
    return (
      <div key={win.id}>

        {display ?
          <div id={win.id}>
            {date}
          </div> :
          null
        }

        <div>
          {`${index + 1}. ${win.text}`}
        </div>
        <Button>Delete</Button>
      </div>
    )
  }
  );

  // while input is focused, on Enter keypress, add small win
  useEffect(() => {
    console.log(winLog);
  }, [winLog])

  return (
    <div className="App">
      <h2>Small Wins</h2>
      <h5>Log your daily achievements, no matter how trivial they first seem</h5>

      <TextField
        id="add-win"
        label="Another small win:"
        variant="standard"
        value={smallWinInput}
        onChange={(ev: React.ChangeEvent<HTMLInputElement>) => handleSetInput(ev.target.value)}
      />
      <Button variant="contained" onClick={handleAddWin}>Add</Button>
      <div>Total wins so far: {winsCount}</div>
      <div>{smallWinsList}</div>
    </div>
  );
}

export default App;
